# development-matplotlib

Nice configuration for paper-quality plots using matplotlib.

The python file includes two functions. Before creating a plot, call `pre_paper_plot()` and after the plot is created, but before showing it, call `post_paper_plot()`.

## Usage and Function Parameters

### pre_paper_plot(change)
  * change: if `True`, change the plot to be paper friendly. `False` disables the paper-friendly plotting.

### post_paper_plot(change, bw_friendly, adjust_splines)
  * change: if `True` (default), change the plot to be paper friendly. `False` disables the changes.
  * bw_friendly: set to `True` to make the plot friendly for black and white printing. Adds different line styles to line-plots.
  * adjust_splines: if `True`, do not display the top and right plot bounding lines (known as splines)

